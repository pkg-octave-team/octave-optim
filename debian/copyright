Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Optim package for Octave
Upstream-Contact: The Octave Community <octave-maintainers@octave.org>
Source: https://gnu-octave.github.io/packages/optim/

Files: *
Copyright: 2002-2008 Etienne Grossmann <etienne@egdn.net>
           2010-2019, 2021-2022 Olaf Till <olaf.till@uni-jena.de>
           2004, 2006-2007, 2010 Michael Creel <michael.creel@uab.es>
           2009 Levente Torok <TorokLev@gmail.com>
           2002 N.J.Higham
           2003 Andy Adler <adler@ncf.ca>
           1992-1994 Arthur Jutan <jutan@charon.engga.uwo.ca>
           1992-1994 Ray Muzic <rfm2@ds2.uh.cwru.edu>
           1992-1994 Richard Shrager
           2007-2019 Andreas Stahel <Andreas.Stahel@bfh.ch>
           2000 Ben Sapp <bsapp@lanl.gov>
           2011 Nir Krakauer <nkrakauer@ccny.cuny.edu>
           2001-2002, 2007 Paul Kienzle <pkienzle@gmail.com>
           2011 Carnë Draug <carandraug+dev@gmail.com>
           2011 Joaquín Ignacio Aramendía <samsagax@gmail.com>
           1996-1997 R. Storn
           2009-2010 Christian Fischer <cfischer@itm.uni-stuttgart.de>
           2011 Fernando Damian Nieuwveldt <fdnieuwveldt@gmail.com>
           2011 Fotios Kasolis <fotios.kasolis@gmail.com>
           2000 Gert Van den Eynde <na.gvandeneynde@na-net.ornl.gov>
           2002 Rolf Fabian <fabian@tu-cottbus.de>
           1994-2015 John W. Eaton <jwe@octave.org>
           2009 Luca Favatella <slackydeb@gmail.com>
           2009 Thomas Treichl <thomas.treichl@gmx.net>
           2008-2009 VZLU Prague, a.s
           2006 Sylvain Pelissier <sylvain.pelissier@gmail.com>
           2012-2014 Rik Wehbring
           2008-2015 Jaroslav Hajek
           2015 Asma Afzal
           2013-2015 Julien Bect
           2000-2015 Gabriele Pannocchia
License: GPL-3+

Files: inst/polyfitinf.m
Copyright: 1998-2011 Andrew V. Knyazev <andrew.knyazev@ucdenver.edu>
License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the <organization> nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: inst/polyconf.m inst/test_wpolyfit.m inst/wpolyfit.m inst/wsolve.m
Copyright: public-domain
License: public-domain
 This program is granted to the public domain.

Files: octave-optim.metainfo.xml
Copyright: 2016 Colin B. Macdonald
License: FSFAP
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.

Files: debian/*
Copyright: 2008 Ólafur Jens Sigurðsson <ojsbug@gmail.com>
           2008-2009, 2012-2022 Rafael Laboissière <rafael@debian.org>
           2009-2011 Thomas Weber <tweber@debian.org>
           2011-2022 Sébastien Villemot <sebastien@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.
